- Current project was created by using Creat-react-app.<br>
- Current application is using React(18.2).<br>
- Redux Tool Kit(1.9.7) is used to store all fetched information in local store.<br>
- React-Router-dom is used to redirect user to appropriated page regarding his loggedIn status. <br>
- Firebase (10.5.2) is used for login/register part and for storing uploaded images. <br>
- Only loggedIn users are able to upload images, rest of users can just see gallery. <br>
- Currently there is no functionality to delete images as well as adding and filtering by tags.
- Current app is stored in Gitlab repository and is available by link: https://gitlab.com/mrTkach/firebase-auth-and-upload <br>
- Current app is deployed to Vercel to be published. and is available by link: https://firebase-auth-and-upload.vercel.app/  <br>
- Styling is very poor just for presentation. Regular CCS was used, no isolated css modules.
- For aligning code and to have it equally aligned across the team I was using prettier, which installed globally. 
- All settings and folder structure can be discussed withing the team during coding or by coding rules document is any.
- I'm completely open for the critic and for the suggestions regarding code base.

The main settings and steps I followed from official documentation is presented by next link and in the screenshots below.
https://firebase.google.com/docs/auth/web/password-auth
<hr>


SIGN-IN:
![img.png](src/assets/images/img.png)
<br />
<br />
<br />
SIGN_UP:
![img_1.png](src/assets/images/img_1.png)
<br />
<br />
<br />
FETCH ALL IMAGES
![img.png](src/assets/images/listAll.png)




<hr>
Aufgabe: <br/>
Entwickle eine Webanwendung, in der man Bilder hochladen und mit Tags versehen kann. <br/>
- Die Bilder sollen persistent gespeichert werden. Die Anzeige der Bilderverwaltung soll in einem Browser erfolgen.<br/>
- Die Anzeige der Bilder soll ohne Anmeldung möglich sein.<br/>
- Das Hochladen und Taggen soll eine Authentifizierung per “basic authentication” erfordern.<br/>
- Freie Technologiewahl
<hr>
 Deine Ergebnisse kannst du gerne Lina zusenden. Wenn noch Fragen oder Unklarheiten bezüglich der Aufgabe bestehen, wende dich gerne an Ole (Ole.Laurisch@engram.de) oder Erik (Erik.legenhausen@engram.de).
<hr>
