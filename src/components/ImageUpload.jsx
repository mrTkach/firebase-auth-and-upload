import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { ref } from "firebase/storage";
import { v4 } from "uuid";
import { uploadImage } from "../redux/galleryActions";
import { storage } from "../firebase.config";

const ImageUpload = () => {
  const [imageUpload, setImageUpload] = useState(null);
  const inputRef = useRef(null);
  const dispatch = useDispatch();
  // ref() creates reference to the image stored in Storage of Firebase
  const imageRef = ref(storage, `image/${imageUpload?.name + v4()}`);
  // v4() from uuid library is used to create unique name for images by avoiding uploading images with the same name

  const uploadImageHandler = () => {
    if (
      imageUpload &&
      (imageUpload?.type === "image/jpeg" || imageUpload?.type === "image/png")
    ) {
      dispatch(uploadImage({ imageUpload, imageRef }));
      setImageUpload(null);
      inputRef.current.value = null;
    }
  };

  const resetInputHandler = () => {
    setImageUpload(null);
    inputRef.current.value = null;
  };

  return (
    <div className="formUpload">
      <input
        ref={inputRef}
        type="file"
        multiple={false}
        accept="image/png, image/gif, image/jpeg"
        onChange={(e) => setImageUpload(e.target.files[0])}
      />
      <button onClick={uploadImageHandler}>Upload</button>
      <button onClick={resetInputHandler}>Reset file input</button>
    </div>
  );
};

export default ImageUpload;
