import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { setUser } from "../redux/slices/userSlice";
import { Form } from "./Form";

const SignUp = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const createUserHandler = (email, password) => {
    const auth = getAuth();

    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        dispatch(
          setUser({
            email: user.email,
            token: user.accessToken,
            id: user.uid,
          })
        );
        localStorage.setItem("loggedUser", JSON.stringify(user));
        localStorage.setItem("isLoggedIn", "true");
        navigate("/");
      })
      .catch((error) => {
        const errorCode = error.code;
        alert(errorCode);
      });
  };

  return (
    <div>
      <Form title="Register" handleClick={createUserHandler} />
    </div>
  );
};

export default SignUp;
