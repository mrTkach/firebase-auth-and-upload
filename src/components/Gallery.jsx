import React, { useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ref } from "firebase/storage";
import { storage } from "../firebase.config";
import { requestAllImage } from "../redux/galleryActions";

const Gallery = () => {
  const dispatch = useDispatch();
  // ref() creates reference to the folder stored in Storage of Firebase
  const imageListRef = ref(storage, "image/");
  const { imageList, status } = useSelector(
    (state) => ({
      imageList: state.gallery.imageList,
      status: state.gallery.status,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(requestAllImage(imageListRef));
  }, []);

  if (status === "loading") {
    return <p>Loading...</p>;
  } else if (status === "finished" && !imageList?.length) {
    return <p>No images to show</p>;
  }

  return (
    <div>
      {imageList?.map((el) => {
        return <img src={el} key={el} alt={el} />;
      })}
    </div>
  );
};

export default Gallery;
