import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { useAuth } from "../hooks/use-auth";
import { removeUser, setUser } from "../redux/slices/userSlice";
import Gallery from "../components/Gallery";
import ImageUpload from "../components/ImageUpload";
import ImageLogo from "../assets/images/logo-navi.svg";

const HomePage = () => {
  const { email } = useAuth();
  const dispatch = useDispatch();
  const [isLoggedIn, setLoggedIn] = useState(false);

  const signOutHandler = () => {
    window.localStorage.removeItem("isLoggedIn");
    window.localStorage.removeItem("loggedUser");
    setLoggedIn(false);
    dispatch(removeUser());
  };

  useEffect(() => {
    try {
      const storageLogin = JSON.parse(localStorage.getItem("isLoggedIn"));
      const loggedUser = JSON.parse(localStorage.getItem("loggedUser"));
      if (storageLogin) {
        setLoggedIn(storageLogin);
        dispatch(
          setUser({
            email: loggedUser.email,
            token: loggedUser.accessToken,
            id: loggedUser.uid,
          })
        );
      }
    } catch (error) {
      console.log("Failed to get login status from local storage", error);
    }
  }, [isLoggedIn]);

  return (
    <div>
      <div className="header">
        <a href="/">
          <img src={ImageLogo} alt="logo" />
        </a>
        <h1>engram coding challenge</h1>

        {isLoggedIn ? (
          <button onClick={signOutHandler}>Log out from {email}</button>
        ) : (
          <>
            <p>
              Please <Link to="/login">Log-in</Link> or{" "}
              <Link to="/register">register</Link>
            </p>
            <p>to be able to upload your own images.</p>
          </>
        )}
      </div>
      {isLoggedIn && <ImageUpload />}
      <Gallery />
    </div>
  );
};

export default HomePage;
