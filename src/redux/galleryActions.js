import { getDownloadURL, listAll, uploadBytes } from "firebase/storage";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const requestAllImage = createAsyncThunk(
  "fetchAllImages",
  async (imageListRef, thunkAPI) => {
    try {
      const response = await listAll(imageListRef);
      const urls = [];
      for (let el of response.items) {
        const url = await getDownloadURL(el);
        urls.push(url);
      }
      return urls;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  },
  {
    // to have action called just once, even in dev mode - adding next condition
    condition: (_, { getState }) => {
      const { status } = getState().gallery;
      if (status === "loading") {
        return false;
      }
    },
  }
);

export const uploadImage = createAsyncThunk(
  "images/upload",
  async ({ imageRef, imageUpload }) => {
    try {
      // uploadBytes() uploads raw bytes to a Cloud Storage file
      // accept 2 arguments - image reference and the image I store in imageUpload (useState())
      const snapshot = await uploadBytes(imageRef, imageUpload);
      // getDownloadURL() gets the URL of a stored file in Firebase Storage
      return await getDownloadURL(snapshot.ref);
    } catch (error) {
      console.error(error);
    }
  }
);
