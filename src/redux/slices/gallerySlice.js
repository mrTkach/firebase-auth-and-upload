import { createSlice } from "@reduxjs/toolkit";
import { requestAllImage, uploadImage } from "../galleryActions";

const initialState = {
  imageList: [],
  status: "idle",
};

const gallerySlice = createSlice({
  name: "gallery",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(requestAllImage.pending, (state) => {
      state.status = "loading";
    });
    builder.addCase(requestAllImage.fulfilled, (state, action) => {
      state.status = "finished";
      state.imageList = action.payload;
    });
    builder.addCase(requestAllImage.rejected, (state) => {
      state.status = "error";
    });
    builder.addCase(uploadImage.pending, (state) => {
      state.status = "loading";
    });
    builder.addCase(uploadImage.fulfilled, (state, action) => {
      state.status = "finished";
      state.imageList.unshift(action.payload);
    });
    builder.addCase(uploadImage.rejected, (state, action) => {
      state.status = "error";
    });
  },
});

export const { setImageList } = gallerySlice.actions;
export default gallerySlice.reducer;
