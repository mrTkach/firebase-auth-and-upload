import { configureStore } from "@reduxjs/toolkit";
import userSliceReducer from "./slices/userSlice";
import gallerySliceReducer from "./slices/gallerySlice";

export const store = configureStore({
  reducer: {
    user: userSliceReducer,
    gallery: gallerySliceReducer,
  },
});
